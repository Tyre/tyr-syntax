use tyr_core::{Entity, Property, PropertyValue};

use constcat::concat;

use std::{collections::BTreeMap, fmt};

const APPROXIMANTS: &str = "yv";
const LIQUIDS: &str = "l";
const NASALS: &str = "nm";
const FRICATIVES: &str = "fszh";
const STOPS: &str = "bcd";

const VOWELS: &str = "iau";
const CONSONANTS: &str = concat!(APPROXIMANTS, LIQUIDS, NASALS, FRICATIVES);

const DELIMITERS: &str = "' \n";
const SEPARATORS: &str = ",;:.!?";

trait At {
    type Output;

    fn at(self, line: usize, column: usize) -> Self::Output;
}

impl<T, E: At> At for Result<T, E> {
    type Output = Result<T, E::Output>;

    #[inline]
    fn at(self, line: usize, column: usize) -> Result<T, E::Output> {
        match self {
            Ok(value) => Ok(value),
            Err(err) => Err(err.at(line, column)),
        }
    }
}

#[derive(Debug)]
pub enum WordKind {
    Object,
    Specifier,
    Relation,
    Modifier,
}

impl fmt::Display for WordKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use WordKind::*;
        match &self {
            Object => write!(f, "object"),
            Specifier => write!(f, "specifier"),
            Relation => write!(f, "relation"),
            Modifier => write!(f, "modifier"),
        }
    }
}

#[derive(Debug)]
pub struct TextError<Kind> {
    kind: Kind,
    line: usize,
    column: usize,
}

impl<Kind: fmt::Display> fmt::Display for TextError<Kind> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}: {}", self.line, self.column, self.kind)
    }
}

#[derive(Debug)]
pub enum ParseErrorKind {
    EmptySentence,
    DelimiterWithoutWord,
    SeparatorAfterDelimiter,
    MissingDelimiter,
    InvalidWord(String),
    InvalidCharacter(char),
    WordsAfterEnd(Vec<String>),
    Unfinished(usize),
    NonExistentWord(String, WordKind),
    ModifierAtStart(String),
}

impl fmt::Display for ParseErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ParseErrorKind::*;
        match &self {
            EmptySentence => write!(f, "The sentence does not contain any words!"),
            DelimiterWithoutWord => write!(f, "Delimiter not after word!"),
            SeparatorAfterDelimiter => write!(f, "Separator after delimiter!"),
            MissingDelimiter => write!(f, "Missing delimiter after separator!"),
            InvalidWord(word) => write!(f, "Invalid phonotactics for word '{word}'!"),
            InvalidCharacter(c) => write!(f, "Character '{c}' is not allowed!"),
            WordsAfterEnd(words) => {
                let mut words = words.iter();
                let first = words.next().cloned().unwrap_or_default();
                write!(
                    f,
                    "The sentence still has words after the last word: {}",
                    words.fold(first, |current, next| format!("{current}, {next}"))
                )
            }
            Unfinished(level) => write!(f, "Sentence is not finished at level {level}!"),
            NonExistentWord(name, kind) => write!(
                f,
                "The {kind} word '{name}' does not exist in this language!"
            ),
            ModifierAtStart(name) => write!(
                f,
                "Modifiers like '{name}' are not allowed at the beginning of a sentence!"
            ),
        }
    }
}

type ParseError = TextError<ParseErrorKind>;

impl At for ParseErrorKind {
    type Output = ParseError;

    fn at(self, line: usize, column: usize) -> ParseError {
        ParseError {
            kind: self,
            line,
            column,
        }
    }
}

fn check_phonotactics(word: &str) -> bool {
    enum WordPhoneme {
        Empty,
        Fricative,
        LiquidOrNasal,
        Approximant,
        Vowel,
        ApproximantOrLiquid,
        NasalOrFricative,
    }

    impl WordPhoneme {
        fn advance(&mut self, c: char) -> bool {
            use WordPhoneme::*;

            *self = if APPROXIMANTS.contains(c) {
                match *self {
                    Empty | Fricative | LiquidOrNasal => Approximant,
                    Approximant => return false,
                    Vowel => ApproximantOrLiquid,
                    ApproximantOrLiquid | NasalOrFricative => return false,
                }
            } else if LIQUIDS.contains(c) {
                match *self {
                    Empty | Fricative => LiquidOrNasal,
                    LiquidOrNasal | Approximant => return false,
                    Vowel => ApproximantOrLiquid,
                    ApproximantOrLiquid | NasalOrFricative => return false,
                }
            } else if NASALS.contains(c) {
                match *self {
                    Empty | Fricative => LiquidOrNasal,
                    LiquidOrNasal | Approximant => return false,
                    Vowel | ApproximantOrLiquid => NasalOrFricative,
                    NasalOrFricative => return false,
                }
            } else if FRICATIVES.contains(c) {
                match *self {
                    Empty => Fricative,
                    Fricative | LiquidOrNasal | Approximant => return false,
                    Vowel | ApproximantOrLiquid => NasalOrFricative,
                    NasalOrFricative => return false,
                }
            } else {
                match *self {
                    Empty | Fricative | LiquidOrNasal | Approximant => Vowel,
                    Vowel | ApproximantOrLiquid | NasalOrFricative => return false,
                }
            };

            true
        }
    }

    let mut current_phoneme = WordPhoneme::Empty;
    let mut chars = word.chars();

    if let Some(first) = chars.next() {
        if !STOPS.contains(first) && !current_phoneme.advance(first) {
            return false;
        }
    }

    for c in chars {
        if !current_phoneme.advance(c) {
            return false;
        }
    }

    true
}

enum SentenceContext<O, S, R, M> {
    Active(SentenceParseContext<O, S, R, M>),
    Finished {
        entity: Entity<Property<O, S, R, M>, O>,
        additional: Vec<String>,
    },
}

impl<O: Clone, S: Clone, R: Clone, M: Clone> SentenceContext<O, S, R, M> {
    fn new() -> Self {
        Self::Active(SentenceParseContext::new())
    }

    fn add_word(
        self,
        name: String,
        has_stop: bool,
        has_prefix: bool,
        has_suffix: bool,
        dictionary: &Dictionary<O, S, R, M>,
    ) -> Result<Self, ParseErrorKind> {
        use crate::SentenceContext::*;

        Ok(match self {
            Active(parse_context) => {
                if !check_phonotactics(&name) {
                    return Err(ParseErrorKind::InvalidWord(name));
                }

                let kind = if has_stop {
                    WordKind::Modifier
                } else {
                    use WordKind::*;
                    match (has_prefix, has_suffix) {
                        (false, false) => return Err(ParseErrorKind::InvalidWord(name)),
                        (true, false) => Specifier,
                        (false, true) => Relation,
                        (true, true) => Object,
                    }
                };

                use WordKind::*;
                match kind {
                    Object => match parse_context.add_object(name, dictionary)? {
                        Ok(context) => Active(context),
                        Err(entity) => Finished {
                            entity,
                            additional: Vec::new(),
                        },
                    },
                    Specifier => Active(parse_context.add_specifier(name, dictionary)?),
                    Relation => Active(parse_context.add_relation(name, dictionary)?),
                    Modifier => Active(parse_context.add_modifier(name, dictionary)?),
                }
            }
            Finished {
                entity,
                mut additional,
            } => {
                additional.push(name);
                Finished { entity, additional }
            }
        })
    }

    fn is_empty(&self) -> bool {
        if let SentenceContext::Active(context) = self {
            context.levels.is_empty() && context.properties.is_empty()
        } else {
            false
        }
    }

    fn finish(self) -> Result<Entity<Property<O, S, R, M>, O>, ParseErrorKind> {
        use crate::SentenceContext::*;
        match self {
            Active(context) => Err(ParseErrorKind::Unfinished(context.levels.len())),
            Finished { entity, additional } => {
                if additional.is_empty() {
                    Ok(entity)
                } else {
                    Err(ParseErrorKind::WordsAfterEnd(additional))
                }
            }
        }
    }
}

pub fn parse<O: Clone, S: Clone, R: Clone, M: Clone>(
    chars: impl Iterator<Item = char>,
    dictionary: &Dictionary<O, S, R, M>,
) -> Result<Vec<Entity<Property<O, S, R, M>, O>>, ParseError> {
    let mut result = Vec::new();
    let mut current_sentence = SentenceContext::new();
    let mut current_word = String::new();
    let mut next_word = String::new();

    let mut has_stop = false;
    let mut has_prefix = false;
    let mut contains_vowel = false;
    let mut ended_sentence = false;
    let mut line = 0;
    let mut column = 0;

    for c in chars {
        if ended_sentence {
            if DELIMITERS.contains(c) {
                ended_sentence = false;
                if c == '\n' {
                    line += 1;
                    column = 0;
                } else {
                    column += 1;
                }
                continue;
            }

            return Err(ParseErrorKind::MissingDelimiter.at(line, column));
        }
        let separator = SEPARATORS.contains(c);
        if separator || DELIMITERS.contains(c) {
            if current_word.is_empty() {
                use ParseErrorKind::*;
                return Err(if separator {
                    if current_sentence.is_empty() {
                        EmptySentence
                    } else {
                        SeparatorAfterDelimiter
                    }
                } else {
                    DelimiterWithoutWord
                }
                .at(line, column));
            }
            let has_suffix = !next_word.is_empty();
            current_sentence = current_sentence
                .add_word(
                    [current_word, next_word].join(""),
                    has_stop,
                    has_prefix,
                    has_suffix,
                    dictionary,
                )
                .at(line, column)?;
            has_prefix = false;
            contains_vowel = false;
            current_word = String::new();
            next_word = String::new();

            if separator {
                result.push(current_sentence.finish().at(line, column)?);
                current_sentence = SentenceContext::new();
                ended_sentence = true;
            }
        } else if STOPS.contains(c) {
            if !current_word.is_empty() {
                if contains_vowel {
                    let has_suffix = !next_word.is_empty();
                    current_sentence = current_sentence
                        .add_word(
                            [current_word, next_word].join(""),
                            has_stop,
                            has_prefix,
                            has_suffix,
                            dictionary,
                        )
                        .at(line, column)?;
                    current_word = String::new();
                    next_word = String::new();
                    contains_vowel = false;
                } else {
                    return Err(
                        ParseErrorKind::InvalidWord([current_word, next_word].join(""))
                            .at(line, column),
                    );
                }
            }

            has_prefix = false;
            has_stop = true;
            current_word.push(c);
        } else if CONSONANTS.contains(c) {
            if contains_vowel {
                next_word.push(c);
            } else {
                current_word.push(c);
            }
        } else if VOWELS.contains(c) {
            if contains_vowel {
                current_sentence = current_sentence
                    .add_word(current_word, has_stop, has_prefix, false, dictionary)
                    .at(line, column)?;
                current_word = next_word;
                next_word = String::new();
                has_stop = false;
            } else {
                contains_vowel = true;
            }
            has_prefix = !current_word.is_empty();
            current_word.push(c);
        } else {
            return Err(ParseErrorKind::InvalidCharacter(c).at(line, column));
        }

        if c == '\n' {
            line += 1;
            column = 0;
        } else {
            column += 1;
        }
    }

    if !current_word.is_empty() {
        let has_suffix = !next_word.is_empty();
        current_sentence = current_sentence
            .add_word(
                [current_word, next_word].join(""),
                has_stop,
                has_prefix,
                has_suffix,
                dictionary,
            )
            .at(line, column)?;
    }

    if !current_sentence.is_empty() {
        result.push(current_sentence.finish().at(line, column)?);
    }

    Ok(result)
}

pub struct Dictionary<O, S, R, M> {
    objects: BTreeMap<String, O>,
    specifiers: BTreeMap<String, S>,
    relations: BTreeMap<String, R>,
    modifiers: BTreeMap<String, M>,
}

impl<O, S, R, M> Dictionary<O, S, R, M> {
    pub fn new(
        objects: BTreeMap<String, O>,
        specifiers: BTreeMap<String, S>,
        relations: BTreeMap<String, R>,
        modifiers: BTreeMap<String, M>,
    ) -> Self {
        Self {
            objects,
            specifiers,
            relations,
            modifiers,
        }
    }
}

struct SentenceParseContext<O, S, R, M> {
    levels: Vec<(R, Vec<M>, Vec<Property<O, S, R, M>>)>,
    properties: Vec<Property<O, S, R, M>>,
}

impl<O: Clone, S: Clone, R: Clone, M: Clone> SentenceParseContext<O, S, R, M> {
    fn new() -> Self {
        Self {
            levels: Vec::new(),
            properties: Vec::new(),
        }
    }

    fn add_object(
        mut self,
        name: String,
        dictionary: &Dictionary<O, S, R, M>,
    ) -> Result<Result<Self, Entity<Property<O, S, R, M>, O>>, ParseErrorKind> {
        if let Some(value) = dictionary.objects.get(&name) {
            let entity = Entity {
                properties: self.properties,
                object: value.clone(),
            };
            Ok(
                if let Some((relation, modifiers, mut stack_properties)) = self.levels.pop() {
                    stack_properties.push(Property::new(PropertyValue::Relative {
                        relation,
                        modifiers,
                        entity,
                    }));
                    self.properties = stack_properties;

                    Ok(self)
                } else {
                    Err(entity)
                },
            )
        } else {
            Err(ParseErrorKind::NonExistentWord(name, WordKind::Object))
        }
    }

    fn add_specifier(
        mut self,
        name: String,
        dictionary: &Dictionary<O, S, R, M>,
    ) -> Result<Self, ParseErrorKind> {
        if let Some(value) = dictionary.specifiers.get(&name) {
            self.properties
                .push(Property::new(PropertyValue::Absolute(value.clone())));
            Ok(self)
        } else {
            Err(ParseErrorKind::NonExistentWord(name, WordKind::Specifier))
        }
    }

    fn add_relation(
        mut self,
        name: String,
        dictionary: &Dictionary<O, S, R, M>,
    ) -> Result<Self, ParseErrorKind> {
        if let Some(value) = dictionary.relations.get(&name) {
            self.levels
                .push((value.clone(), Vec::new(), self.properties));
            self.properties = Vec::new();
            Ok(self)
        } else {
            Err(ParseErrorKind::NonExistentWord(name, WordKind::Relation))
        }
    }

    fn add_modifier(
        mut self,
        name: String,
        dictionary: &Dictionary<O, S, R, M>,
    ) -> Result<Self, ParseErrorKind> {
        if let Some(value) = dictionary.modifiers.get(&name) {
            if let Some(property) = self.properties.last_mut() {
                property.modifiers.push(value.clone());
                Ok(self)
            } else if let Some((_, modifiers, _)) = self.levels.last_mut() {
                modifiers.push(value.clone());
                Ok(self)
            } else {
                Err(ParseErrorKind::ModifierAtStart(name))
            }
        } else {
            Err(ParseErrorKind::NonExistentWord(name, WordKind::Modifier))
        }
    }
}
